import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay16 {

    @Test
    fun runExample1() {

        val exampleInput = stringListFromString("""
            |Before: [3, 2, 1, 1]
            |9 2 1 2
            |After:  [3, 2, 2, 1]
            |""".trimMargin(), LINE)

        Assertions
                .assertThat(Day16.runPart1(exampleInput))
                .isEqualTo(1)
    }

    @Test
    fun runPuzzle1() {
        val result = Day16.runPart1(stringListFromFile("16-1", LINE))
        println("Puzzle 16-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(542)
    }

    @Test
    fun runPuzzle2() {
        val result = Day16.runPart2(stringListFromFile("16-2", LINE))
        println("Puzzle 16-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(575)
    }
}