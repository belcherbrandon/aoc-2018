import Utils.stringFromFile
import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay5 {

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day5.runPart1("aA"))
                .isEqualTo(0)

        Assertions
                .assertThat(Day5.runPart1("abBA"))
                .isEqualTo(0)

        Assertions
                .assertThat(Day5.runPart1("abAB"))
                .isEqualTo(4)

        Assertions
                .assertThat(Day5.runPart1("aabAAB"))
                .isEqualTo(6)

        Assertions
                .assertThat(Day5.runPart1("dabAcCaCBAcCcaDA"))
                .isEqualTo(10)
    }

    @Test
    fun runPuzzle1() {
        val result = Day5.runPart1(stringFromFile("5-1"))
        println("Puzzle 5-1: $result")
        Assertions
                .assertThat(result)
                .isNotEqualTo(11669)
                .isEqualTo(11668)
    }

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day5.runPart2("dabAcCaCBAcCcaDA"))
                .isEqualTo(4)
    }

    @Test
    fun runPuzzle2() {
        val result = Day5.runPart2(stringFromFile("5-1"))
        println("Puzzle 5-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(4652)
    }
}