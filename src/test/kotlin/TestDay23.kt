import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay23 {

    @Test
    fun runExample1() {
        val exampleInput = stringListFromString("""
            |pos=<0,0,0>, r=4
            |pos=<1,0,0>, r=1
            |pos=<4,0,0>, r=3
            |pos=<0,2,0>, r=1
            |pos=<0,5,0>, r=3
            |pos=<0,0,3>, r=1
            |pos=<1,1,1>, r=1
            |pos=<1,1,2>, r=1
            |pos=<1,3,1>, r=1""".trimMargin(), LINE)

        Assertions
                .assertThat(Day23.runPart1(exampleInput))
                .isEqualTo(7)

    }

    @Test
    fun runPuzzle1() {
        val result = Day23.runPart1(stringListFromFile("23-1", LINE))
        println("Puzzle 23-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(410)
    }

    @Test
    fun runExample2() {
        val exampleInput = stringListFromString("""
            |pos=<10,12,12>, r=2
            |pos=<12,14,12>, r=2
            |pos=<16,12,12>, r=4
            |pos=<14,14,14>, r=6
            |pos=<50,50,50>, r=200
            |pos=<10,10,10>, r=5""".trimMargin(), LINE)

        Assertions
                .assertThat(Day23.runPart2(exampleInput))
                .isEqualTo(7)
    }

    @Test
    fun runPuzzle2() {
        val result = Day23.runPart2(stringListFromFile("23-1", LINE))
        println("Puzzle 23-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(410)
    }
}