import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay7 {

    private val exampleInput = stringListFromString("""
        |Step C must be finished before step A can begin.
        |Step C must be finished before step F can begin.
        |Step A must be finished before step B can begin.
        |Step A must be finished before step D can begin.
        |Step B must be finished before step E can begin.
        |Step D must be finished before step E can begin.
        |Step F must be finished before step E can begin.""".trimMargin(), LINE)

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day7.runPart1(exampleInput))
                .isEqualTo("CABDFE")
    }

    @Test
    fun runPuzzle1() {
        val result = Day7.runPart1(stringListFromFile("7-1", LINE))
        println("Puzzle 7-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo("LAPFCRGHVZOTKWENBXIMSUDJQY")
    }

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day7.runPart2(exampleInput, 2, 0))
                .isEqualTo(15)
    }

    @Test
    fun runPuzzle2() {
        val result = Day7.runPart2(stringListFromFile("7-1", LINE), 5, 60)
        println("Puzzle 7-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(936)
    }

    @Test
    fun testNodeWeight() {
        val result = Day7.getNodeEffort('A')
        Assertions
                .assertThat(result)
                .isEqualTo(1)
    }
}