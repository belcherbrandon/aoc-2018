import Utils.stringFromFile
import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay12 {

    private val exampleInput = stringListFromString("""
        |...## => #
        |..#.. => #
        |.#... => #
        |.#.#. => #
        |.#.## => #
        |.##.. => #
        |.#### => #
        |#.#.# => #
        |#.### => #
        |##.#. => #
        |##.## => #
        |###.. => #
        |###.# => #
        |####. => #""".trimMargin(), LINE)

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day12.runPart1("#..#.#..##......###...###", exampleInput))
                .isEqualTo(325)
    }

    @Test
    fun runTest() {
        Day12.runPart1("#.#.....#.#.....#.#.........#.#.....#.#..........#.#.....#.#....#.#........#.#......#.#", exampleInput)
    }

    @Test
    fun runPuzzle1() {
        val result = Day12.runPart1(stringFromFile("12-1a"), stringListFromFile("12-1b", LINE))
        println("Puzzle 12-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(3059)
    }

    @Test
    fun runPuzzle2() {
        val result = Day12.runPart2(stringFromFile("12-1a"), stringListFromFile("12-1b", LINE))
        println("Puzzle 12-2: $result")
        Assertions
                .assertThat(result)
                .isGreaterThan(74776)
                .isNotEqualTo(3650000010317)
                .isNotEqualTo(3650000001849)
                .isEqualTo(3650000001776)
    }
}