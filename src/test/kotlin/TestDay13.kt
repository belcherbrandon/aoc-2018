import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay13 {

    private val exampleInput = stringListFromString("""
        */->-\
        *|   |  /----\
        *| /-+--+-\  |
        *| | |  | v  |
        *\-+-/  \-+--/
        *  \------/   """.trimMargin("*"), LINE)

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day13.runPart1(exampleInput))
                .isEqualTo("7,3")
    }

    @Test
    fun runPuzzle1() {
        val result = Day13.runPart1(stringListFromFile("13-1", LINE))
        println("Puzzle 13-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo("83,49")
    }

    private val exampleInput2 = stringListFromString("""
        */>-<\
        *|   |
        *| /<+-\
        *| | | v
        *\>+</ |
        *  |   ^
        *  \<->/""".trimMargin("*"), LINE)

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day13.runPart2(exampleInput2))
                .isEqualTo("6,4")
    }

    @Test
    fun runPuzzle2() {
        val result = Day13.runPart2(stringListFromFile("13-1", LINE))
        println("Puzzle 13-2: $result")
        Assertions
                .assertThat(result)
                .isNotEqualTo("14,134")
                .isEqualTo("73,36")
    }

    @Test
    fun testEnum() {
        Assertions
                .assertThat(Day13.Cardinal.N.right())
                .isEqualTo(Day13.Cardinal.E)

        Assertions
                .assertThat(Day13.Cardinal.W.left())
                .isEqualTo(Day13.Cardinal.S)

        Assertions
                .assertThat(Day13.Cardinal.N.left())
                .isEqualTo(Day13.Cardinal.W)
    }
}