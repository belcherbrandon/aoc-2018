import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay15 {

    @Test
    fun runExample1() {

        val exampleInput1 = stringListFromString("""
            |#######
            |#.G...#
            |#...EG#
            |#.#.#G#
            |#..G#E#
            |#.....#
            |#######""".trimMargin(), LINE)

        Assertions
                .assertThat(Day15.runPart1(exampleInput1))
                .isEqualTo(27730)
    }

    @Test
    fun runExample2() {

        val exampleInput1 = stringListFromString("""
            |#########
            |#G..G..G#
            |#.......#
            |#.......#
            |#G..E..G#
            |#.......#
            |#.......#
            |#G..G..G#
            |#########""".trimMargin(), LINE)

        Assertions
                .assertThat(Day15.runPart1(exampleInput1))
                .isEqualTo(27730)
    }

    @Test
    fun runExample3() {

        val exampleInput1 = stringListFromString("""
            |#########
            |#.G...G.#
            |#...G...#
            |#...E...#
            |#.G...G.#
            |#.......#
            |#.......#
            |#G..G..G#
            |#########""".trimMargin(), LINE)

        Assertions
                .assertThat(Day15.testMinDistance(exampleInput1))
                .isEqualTo(27730)
    }

    @Test
    fun runExample4() {

        val exampleInput1 = stringListFromString("""
            |#######
            |#G..#E#
            |#E#E.E#
            |#G.##.#
            |#...#E#
            |#...E.#
            |#######""".trimMargin(), LINE)

        Assertions
                .assertThat(Day15.runPart1(exampleInput1))
                .isEqualTo(36334)
    }

    @Test
    fun runExample5() {

        val exampleInput1 = stringListFromString("""
            |#######
            |#E..EG#
            |#.#G.E#
            |#E.##E#
            |#G..#.#
            |#..E#.#
            |#######""".trimMargin(), LINE)

        Assertions
                .assertThat(Day15.runPart1(exampleInput1))
                .isEqualTo(39514)
    }

    @Test
    fun runPuzzle1() {
        val result = Day15.runPart1(stringListFromFile("15-1", LINE))
        println("Puzzle 15-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo("3656126723")
    }
}