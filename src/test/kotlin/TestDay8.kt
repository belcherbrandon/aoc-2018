import Utils.intArrayFromFile
import Utils.intArrayFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay8 {

    private val exampleInput = intArrayFromString("2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2", SPACE)

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day8.runPart1(exampleInput))
                .isEqualTo(138)
    }

    @Test
    fun runExample1b() {
        Assertions
                .assertThat(Day8.runPart1(intArrayFromString("2 3 1 1 0 1 99 2 0 3 10 11 12 1 1 2", SPACE)))
                .isEqualTo(138)
    }

    @Test
    fun runPuzzle1() {
        val result = Day8.runPart1(intArrayFromFile("8-1", SPACE))
        println("Puzzle 8-1: $result")
        Assertions
                .assertThat(result)
                .isGreaterThan(17683)
                .isGreaterThan(17724)
                .isEqualTo(47647)

    }

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day8.runPart2(exampleInput))
                .isEqualTo(66)
    }

    @Test
    fun runPuzzle2() {
        val result = Day8.runPart2(intArrayFromFile("8-1", SPACE))
        println("Puzzle 8-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(23636)
    }
}