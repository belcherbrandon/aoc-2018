import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay17 {

    @Test
    fun runExample1() {

        val exampleInput = stringListFromString("""
            |x=495, y=2..7
            |y=7, x=495..501
            |x=501, y=3..7
            |x=498, y=2..4
            |x=506, y=1..2
            |x=498, y=10..13
            |x=504, y=10..13
            |y=13, x=498..504""".trimMargin(), LINE)

        Assertions
                .assertThat(Day17.runPart1(exampleInput, true))
                .isEqualTo(57)
    }

    @Test
    fun runExample2() {

        val exampleInput = stringListFromString("""
            |x=493, y=3..10
            |x=505, y=3..10
            |x=493..505, y=11
            |x=495, y=6..7
            |x=499, y=6..7
            |x=495..499, y=8""".trimMargin(), LINE)

        Assertions
                .assertThat(Day17.runPart1(exampleInput, true))
                .isEqualTo(97)
    }

    @Test
    fun runExample3() {
        val exampleInput = stringListFromString("""
            |x=493, y=3..10
            |x=505, y=3..10
            |x=493..505, y=11
            |x=495, y=6..7
            |x=503, y=6..7
            |x=495..503, y=8""".trimMargin(), LINE)

        Assertions
                .assertThat(Day17.runPart1(exampleInput, true))
                .isEqualTo(93)
    }

    @Test
    fun runExample4() {
        val exampleInput = stringListFromString("""
            |x=493, y=3..10
            |x=505, y=3..10
            |x=493..505, y=11
            |x=495, y=2..7
            |x=503, y=2..7
            |x=495..503, y=8""".trimMargin(), LINE)

        Assertions
                .assertThat(Day17.runPart1(exampleInput, true))
                .isEqualTo(100)
    }

    @Test
    fun runPuzzle1() {
        val result = Day17.runPart1(stringListFromFile("17-1", LINE), false)
        println("Puzzle 17-1: $result")
        Assertions
                .assertThat(result)
                .isGreaterThan(3643)
                .isNotEqualTo(44748)
                .isNotEqualTo(44746)
                .isLessThan(44747)
                .isEqualTo(44743)
    }

    @Test
    fun runPuzzle2() {
        val result = Day17.runPart2(stringListFromFile("17-1", LINE), false)
        println("Puzzle 17-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(34172)
    }

}