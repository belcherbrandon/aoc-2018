import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay18 {

    @Test
    fun runExample1() {

        val exampleInput = stringListFromString("""
            -.#.#...|#.
            -.....#|##|
            -.|..|...#.
            -..|#.....#
            -#.#|||#|#|
            -...#.||...
            -.|....|...
            -||...#|.#|
            -|.||||..|.
            -...#.|..|.""".trimMargin("-"), LINE)

        Assertions
                .assertThat(Day18.runPart1(exampleInput))
                .isEqualTo(1147)
    }


    @Test
    fun runPuzzle1() {
        val result = Day18.runPart1(stringListFromFile("18-1", LINE))
        println("Puzzle 18-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(531417)
    }

    // puzzle2
    // point of repeat + (index - point of repeat % cycle length)
    // 1632 + (1000000000 - 1632 % 28) = 1644
    // index 1644 has a value of 205296
}