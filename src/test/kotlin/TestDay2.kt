import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay2 {

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day2.runPart1(stringListFromString(
                        """
                        |abcdef
                        |bababc
                        |abbcde
                        |abcccd
                        |aabcdd
                        |abcdee
                        |ababab
                        """.trimMargin(), LINE)))
                .isEqualTo(12)
    }

    @Test
    fun runPuzzle1() {
        val result = Day2.runPart1(stringListFromFile("2-1", LINE))
        println("Puzzle 2-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(6888)
    }

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day2.runPart2(stringListFromString(
                        """
                        |abcde
                        |fghij
                        |klmno
                        |pqrst
                        |fguij
                        |axcye
                        |wvxyz
                        """.trimMargin(), LINE)))
                .isEqualTo("fgij")
    }

    @Test
    fun runPuzzle2() {
        val result = Day2.runPart2(stringListFromFile("2-1", LINE))
        println("Puzzle 2-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo("icxjvbrobtunlelzpdmfkahgs")
    }

}