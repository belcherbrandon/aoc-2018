import org.junit.jupiter.api.Test
import Utils.intArrayFromString
import Utils.intArrayFromFile
import org.assertj.core.api.Assertions

class TestDay1 {

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day1.runPart1(intArrayFromString("+1, +1, +1", COMMA)))
                .isEqualTo(3)

        Assertions
                .assertThat(Day1.runPart1(intArrayFromString("+1, +1, -2", COMMA)))
                .isEqualTo(0)

        Assertions
                .assertThat(Day1.runPart1(intArrayFromString("-1, -2, -3", COMMA)))
                .isEqualTo(-6)
    }

    @Test
    fun runPuzzle1() {
        val result = Day1.runPart1(intArrayFromFile("1-1", LINE))
        println("Puzzle 1-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(590)
    }

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day1.runPart2(intArrayFromString("1, -1", COMMA)))
                .isEqualTo(0)

        Assertions
                .assertThat(Day1.runPart2(intArrayFromString("+3, +3, +4, -2, -4", COMMA)))
                .isEqualTo(10)

        Assertions
                .assertThat(Day1.runPart2(intArrayFromString("-6, +3, +8, +5, -6", COMMA)))
                .isEqualTo(5)
    }

    @Test
    fun runPuzzle2() {
        val result = Day1.runPart2(intArrayFromFile("1-1", LINE))
        println("Puzzle 1-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(83445)
    }

}
