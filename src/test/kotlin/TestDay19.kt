import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay19 {

    @Test
    fun runExample1() {

        val exampleInput = stringListFromString("""
            |seti 5 0 1
            |seti 6 0 2
            |addi 0 1 0
            |addr 1 2 3
            |setr 1 0 0
            |seti 8 0 4
            |seti 9 0 5""".trimMargin(), LINE)

        Assertions
                .assertThat(Day19.runPart1(exampleInput))
                .isEqualTo(7)
    }


    @Test
    fun runPuzzle1() {
        val result = Day19.runPart1(stringListFromFile("19-1", LINE))
        println("Puzzle 19-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(1530)
    }

    @Test
    fun runPuzzle2() {
        var v0 = 1
        var v2 = 0
        var v5 = 0

        v2 += 2   // 17
        v2 *= v2  // 18
        v2 *= 19  // 19
        v2 *= 11  // 20
        v5 += 8   // 21
        v5 *= 22  // 22
        v5 += 6   // 23
        v2 += v5  // 24

        if (v0 == 1) {
            v5 = 27
            v5 *= 28
            v5 += 29
            v5 *= 30
            v5 *= 14
            v5 *= 32
            v2 += v5
            v0 = 0
        }

        var v1 = 1   // 1

        do {
            var v4 = 1   // 2

            do {

                if (v1 * v4 == v2) {  // 3 - 7
                    v0 += v1
                }

                v4++  // 8

            } while (v4 <= v2)

            v1++  // 12

        } while (v1 <= v2)

        // exit 16

        val result = v0

        println("Puzzle 19-2: $result")
        Assertions
                .assertThat(result)
                .isGreaterThan(257)
                .isNotEqualTo(10551418)
                .isNotEqualTo(1531)
                .isEqualTo(16533000)
    }
}