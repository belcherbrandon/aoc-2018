import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay11 {

    @Test
    fun powerLevelCalc1() {
        Assertions
                .assertThat(Day11.calculatePowerLevel(3, 5, 8))
                .isEqualTo(4)
    }

    @Test
    fun powerLevelCalc2() {
        Assertions
                .assertThat(Day11.calculatePowerLevel(122, 79, 57))
                .isEqualTo(-5)
    }

    @Test
    fun powerLevelCalc3() {
        Assertions
                .assertThat(Day11.calculatePowerLevel(217, 196, 39))
                .isEqualTo(0)
    }

    @Test
    fun powerLevelCalc4() {
        Assertions
                .assertThat(Day11.calculatePowerLevel(101, 153, 71))
                .isEqualTo(4)
    }

    @Test
    fun runExample1a() {
        Assertions
                .assertThat(Day11.runPart1(18, 3..3))
                .isEqualTo("33,45,3")
    }

    @Test
    fun runExample1b() {
        Assertions
                .assertThat(Day11.runPart1(42, 3..3))
                .isEqualTo("21,61,3")
    }

    @Test
    fun runPuzzle1() {
        val result = Day11.runPart1(5468, 3..3)
        println("Puzzle 11-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo("243,64,3")
    }

    @Test
    fun runExample2a() {
        Assertions
                .assertThat(Day11.runPart1(18, 1..300))
                .isEqualTo("90,269,16")
    }

    @Test
    fun runExample2b() {
        Assertions
                .assertThat(Day11.runPart1(42, 1..300))
                .isEqualTo("232,251,12")
    }

    @Test
    fun runPuzzle2() {
        val result = Day11.runPart1(5468, 1..300)
        println("Puzzle 11-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo("90,101,15")
    }
}