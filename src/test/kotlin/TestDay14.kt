import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay14 {

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day14.runPart1(9))
                .isEqualTo("5158916779")

        Assertions
                .assertThat(Day14.runPart1(5))
                .isEqualTo("0124515891")

        Assertions
                .assertThat(Day14.runPart1(18))
                .isEqualTo("9251071085")

        Assertions
                .assertThat(Day14.runPart1(2018))
                .isEqualTo("5941429882")
    }

    @Test
    fun runPuzzle1() {
        val result = Day14.runPart1(880751)
        println("Puzzle 14-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo("3656126723")
    }

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day14.runPart2(100, "51589"))
                .isEqualTo(9)

        Assertions
                .assertThat(Day14.runPart2(100, "01245"))
                .isEqualTo(5)

        Assertions
                .assertThat(Day14.runPart2(100, "92510"))
                .isEqualTo(18)

        Assertions
                .assertThat(Day14.runPart2(2500, "59414"))
                .isEqualTo(2018)
    }

    @Test
    fun runPuzzle2() {
        val result = Day14.runPart2(100000000, "880751")
        println("Puzzle 14-2: $result")
        Assertions
                .assertThat(result)
                .isGreaterThan(36561)
                .isEqualTo(20333868)
    }
}