import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay9 {

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day9.runPart1(9, 25))
                .isEqualTo(32)

        Assertions
                .assertThat(Day9.runPart1(9, 46))
                .isEqualTo(63)

        Assertions
                .assertThat(Day9.runPart1(10, 1618))
                .isEqualTo(8317)

        Assertions
                .assertThat(Day9.runPart1(13, 7999))
                .isEqualTo(146373)

        Assertions
                .assertThat(Day9.runPart1(17, 1104))
                .isEqualTo(2764)

        Assertions
                .assertThat(Day9.runPart1(21, 6111))
                .isEqualTo(54718)

        Assertions
                .assertThat(Day9.runPart1(30, 5807))
                .isEqualTo(37305)
    }

    @Test
    fun runPuzzle1() {
        val result = Day9.runPart1(441, 71032)
        println("Puzzle 9-1: $result")
        Assertions
                .assertThat(result)
                .isLessThan(160435597)
                .isEqualTo(393229)

    }

    @Test
    fun runPuzzle2() {
        val result = Day9.runPart1(441, 71032 * 100)
        println("Puzzle 9-2: $result")
        Assertions
                .assertThat(result)
                .isGreaterThan(33559649)
                .isEqualTo(3273405195L)
    }
}