import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay3 {

    private val exampleInput = stringListFromString("""
        |#1 @ 1,3: 4x4
        |#2 @ 3,1: 4x4
        |#3 @ 5,5: 2x2""".trimMargin(), LINE)

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day3.runPart1(exampleInput))
                .isEqualTo(4)
    }

    @Test
    fun runPuzzle1() {
        val result = Day3.runPart1(stringListFromFile("3-1", LINE))
        println("Puzzle 3-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(113576)
    }

    @Test
    fun runExample3() {
        Assertions
                .assertThat(Day3.runPart2(exampleInput))
                .isEqualTo(3)
    }

    @Test
    fun runPuzzle3() {
        val result = Day3.runPart2(stringListFromFile("3-1", LINE))
        println("Puzzle 3-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(825)
    }
}