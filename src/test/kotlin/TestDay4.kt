import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay4 {

    private val exampleInput = stringListFromString("""
        |[1518-11-01 00:00] Guard #10 begins shift
        |[1518-11-01 00:05] falls asleep
        |[1518-11-01 00:25] wakes up
        |[1518-11-01 00:30] falls asleep
        |[1518-11-01 00:55] wakes up
        |[1518-11-01 23:58] Guard #99 begins shift
        |[1518-11-02 00:40] falls asleep
        |[1518-11-02 00:50] wakes up
        |[1518-11-03 00:05] Guard #10 begins shift
        |[1518-11-03 00:24] falls asleep
        |[1518-11-03 00:29] wakes up
        |[1518-11-04 00:02] Guard #99 begins shift
        |[1518-11-04 00:36] falls asleep
        |[1518-11-04 00:46] wakes up
        |[1518-11-05 00:03] Guard #99 begins shift
        |[1518-11-05 00:45] falls asleep
        |[1518-11-05 00:55] wakes up""".trimMargin(), LINE)

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day4.runPart1(exampleInput))
                .isEqualTo(240)
    }

    @Test
    fun runPuzzle1() {
        val result = Day4.runPart1(stringListFromFile("4-1", LINE))
        println("Puzzle 4-1: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(71748)
    }

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day4.runPart2(exampleInput))
                .isEqualTo(4455)
    }

    @Test
    fun runPuzzle2() {
        val result = Day4.runPart2(stringListFromFile("4-1", LINE))
        println("Puzzle 4-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(106850)
    }
}