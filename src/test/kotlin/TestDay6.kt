import Utils.stringListFromFile
import Utils.stringListFromString
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class TestDay6 {

    private val exampleInput = stringListFromString("""
        |1, 1
        |1, 6
        |8, 3
        |3, 4
        |5, 5
        |8, 9""".trimMargin(), LINE)

    @Test
    fun runExample1() {
        Assertions
                .assertThat(Day6.runPart1(exampleInput))
                .isEqualTo(17)
    }

    @Test
    fun runPuzzle1() {
        val result = Day6.runPart1(stringListFromFile("6-1", LINE))
        println("Puzzle 6-1: $result")
        Assertions
                .assertThat(result)
                .isLessThan(6122)
                .isEqualTo(6047)
    }

    @Test
    fun runExample2() {
        Assertions
                .assertThat(Day6.runPart2(exampleInput, 32))
                .isEqualTo(16)
    }

    @Test
    fun runPuzzle2() {
        val result = Day6.runPart2(stringListFromFile("6-1", LINE), 10000)
        println("Puzzle 6-2: $result")
        Assertions
                .assertThat(result)
                .isEqualTo(46320)
    }
}