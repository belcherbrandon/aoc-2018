object Day19 {

    fun runPart1(inputs: List<String>): Int {

        val instructions = inputs.map {
            val split = it.split(" ")
            if (split.size > 2) {
                Instruction(split[0], split[1].toInt(), split[2].toInt(), split[3].toInt())
            } else {
                Instruction(split[0], split[1].toInt())
            }
        }

        val registers = Device().start(3, instructions)
        
        return registers[0]
    }

    class Device {
        var ipRegister = 0

        val registers = intArrayOf(1, 0, 0, 0, 0, 0)

        fun start(ipRegister: Int, instructions: List<Instruction>): IntArray {
            this.ipRegister = ipRegister

            while (ip >= 0 && ip < instructions.size) {
                instructions[ip].execute(registers)
                ip++
            }

            return registers
        }

        var ip: Int
            get() = registers[ipRegister]
            set(value) {
                registers[ipRegister] = value
            }
    }

    class Instruction(val type: String, val first: Int, private val second: Int, private val third: Int) {
        
        constructor(type: String, first: Int) : this(type, first, -1, -1)

        fun execute(registers: IntArray) {
            when (type) {
                "addr" -> addr(registers)
                "addi" -> addi(registers)
                "mulr" -> mulr(registers)
                "muli" -> muli(registers)
                "banr" -> banr(registers)
                "bani" -> bani(registers)
                "borr" -> borr(registers)
                "bori" -> bori(registers)
                "setr" -> setr(registers)
                "seti" -> seti(registers)
                "gtir" -> gtir(registers)
                "gtri" -> gtri(registers)
                "gtrr" -> gtrr(registers)
                "eqir" -> eqir(registers)
                "eqri" -> eqri(registers)
                "eqrr" -> eqrr(registers)
            }
            registers
        }

        private fun addr(registers: IntArray) {
            registers[third] = registers[first] + registers[second]
        }

        private fun addi(registers: IntArray) {
            registers[third] = registers[first] + second
        }

        private fun mulr(registers: IntArray) {
            registers[third] = registers[first] * registers[second]
        }

        private fun muli(registers: IntArray) {
            registers[third] = registers[first] * second
        }

        private fun banr(registers: IntArray) {
            registers[third] = registers[first] and registers[second]
        }

        private fun bani(registers: IntArray) {
            registers[third] = registers[first] and second
        }

        private fun borr(registers: IntArray) {
            registers[third] = registers[first] or registers[second]
        }

        private fun bori(registers: IntArray) {
            registers[third] = registers[first] or second
        }

        private fun setr(registers: IntArray) {
            registers[third] = registers[first]
        }

        private fun seti(registers: IntArray) {
            registers[third] = first
        }

        private fun gtir(registers: IntArray) {
            if (first > registers[second]) {
                registers[third] = 1
            } else {
                registers[third] = 0
            }
        }

        private fun gtri(registers: IntArray) {
            if (registers[first] > second) {
                registers[third] = 1
            } else {
                registers[third] = 0
            }
        }

        private fun gtrr(registers: IntArray) {
            if (registers[first] > registers[second]) {
                registers[third] = 1
            } else {
                registers[third] = 0
            }
        }

        private fun eqir(registers: IntArray) {
            if (first == registers[second]) {
                registers[third] = 1
            } else {
                registers[third] = 0
            }
        }

        private fun eqri(registers: IntArray) {
            if (registers[first] == second) {
                registers[third] = 1
            } else {
                registers[third] = 0
            }
        }

        private fun eqrr(registers: IntArray) {
            if (registers[first] == registers[second]) {
                registers[third] = 1
            } else {
                registers[third] = 0
            }
        }
    }
}