object Day3 {

    fun runPart1(input: List<String>) : Int {
        val grid = Array(1000) { Array(1000) { ArrayList<String>() } }
        populateGrid(grid, input)
        return calculateOverlap(grid)
    }

    fun runPart2(input: List<String>) : Int {
        val grid = Array(1000) { Array(1000) { ArrayList<String>() } }
        populateGrid(grid, input)
        return findWholeClaim(grid)
    }

    private fun findWholeClaim(grid: Grid): Int {
        val overlappedClaims = HashSet<String>()
        for (i in 0 until grid.size) {
            for (j in 0 until grid[i].size) {
                if (grid[i][j].size > 1) {
                    overlappedClaims.addAll(grid[i][j])
                }
            }
        }

        for (k in 1..1365) {
            if (!overlappedClaims.contains("#$k")) {
                return k
            }
        }

        return -1
    }

    private fun populateGrid(grid: Grid, input: List<String>) {
        input.forEach {
            val splitString = it.split(" @ ", ",", ": ", "x")
            val claim = Claim(splitString[0], splitString[1].toInt(), splitString[2].toInt(), splitString[3].toInt(), splitString[4].toInt())
            makeClaim(grid, claim)
        }
    }

    private fun makeClaim(grid: Grid, claim: Claim) {
        for (i in claim.x until claim.x + claim.width) {
            for (j in claim.y until claim.y + claim.height) {
                grid[i][j].add(claim.id)
            }
        }
    }

    private fun calculateOverlap(grid: Grid): Int {
        var count = 0
        for (i in 0 until grid.size) {
            for (j in 0 until grid[i].size) {
                if (grid[i][j].size > 1) {
                    count++
                }
            }
        }
        return count
    }

    private class Claim(val id: String, val x: Int, val y: Int, val width: Int, val height: Int)
}
