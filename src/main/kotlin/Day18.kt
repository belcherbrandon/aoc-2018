object Day18 {

    fun runPart1(inputs: List<String>): Int {

        val width = inputs[0].length
        val height = inputs.size

        var board = Array(width) { Array(height) { '.' } }

        inputs.forEachIndexed { y, row ->
            row.forEachIndexed { x, cell ->
                board[x][y] = cell
            }
        }

        printBoard(board)

        val history = mutableListOf<Int>()

        for (i in 1..100000) {
            board = mutateBoard(board, width, height)

            if (i >= 1632) {
                val tally = tally(board)
                if (history.contains(tally)) {
                    return tally
                } else {
                    println("$i : $tally")
                    printBoard(board)
                    history.add(tally)
                }
            }
            //printBoard(board)
        }

        printBoard(board)

        return tally(board)
    }

    private fun tally(board: Array<Array<Char>>): Int {
        // Tally
        var numTrees = 0
        var numYards = 0
        board.forEachIndexed { y, row ->
            row.forEachIndexed { x, cell ->
                if (board[x][y] == '|') {
                    numTrees++
                } else if (board[x][y] == '#') {
                    numYards++
                }
            }
        }

        return numTrees * numYards
    }

    private fun printBoard(board: Array<Array<Char>>) {
        board.forEachIndexed { y, row ->
            row.forEachIndexed { x, cell ->
                print( board[x][y])
            }
            println()
        }

        println()
    }

    private fun mutateBoard(board: Array<Array<Char>>, width: Int, height: Int): Array<Array<Char>> {
        val newBoard = Array( width ) { Array( height ) { '.' } }
        board.forEachIndexed { y, row ->
            row.forEachIndexed { x, cell ->
                newBoard[x][y] = mutateCell(board, width, height, x, y)
            }
        }
        return newBoard
    }

    private fun mutateCell(board: Array<Array<Char>>, width: Int, height: Int, x: Int, y: Int): Char {
        val neighbours = mutableListOf<Char>()

        if (x - 1 >= 0 && y - 1 >= 0) {
            neighbours.add(board[x - 1][y - 1])
        }
        if (y - 1 >= 0) {
            neighbours.add(board[x][y - 1])
        }
        if (x + 1 < width && y - 1 >= 0) {
            neighbours.add(board[x + 1][y - 1])
        }

        if (x - 1 >= 0) {
            neighbours.add(board[x - 1][y])
        }
        if (x + 1 < width) {
            neighbours.add(board[x + 1][y])
        }

        if (x - 1 >= 0 && y + 1 < height) {
            neighbours.add(board[x - 1][y + 1])
        }
        if (y + 1 < height) {
            neighbours.add(board[x][y + 1])
        }
        if (x + 1 < width && y + 1 < height) {
            neighbours.add(board[x + 1][y + 1])
        }

        val counts = neighbours.groupingBy { it }.eachCount()
        val currentState = board[x][y]
        val numTrees = counts.getOrDefault('|', 0)
        val numYards = counts.getOrDefault('#', 0)

        return if (currentState == '.' && numTrees >= 3) {
            '|'
        } else if (currentState == '|' && numYards >= 3) {
            '#'
        } else if (currentState == '#' && !(numYards >= 1 && numTrees >= 1)) {
            '.'
        } else {
            currentState
        }
    }

}