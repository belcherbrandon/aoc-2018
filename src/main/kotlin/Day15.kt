object Day15 {

    fun runPart1(mapLayout: List<String>): Int {
        val state = createInitialState(mapLayout)

        printBoard(state)
        while (state.elves.size > 0 && state.goblins.size > 0) {

            state.board.values.forEach {
                if (it.unit != null && it.lastProcessed != state.round) {
                    executeTurn(state, it)
                }
            }

            printBoard(state)
            state.round++
        }

        return --state.round * (state.elves.sumBy { it.hp } + state.goblins.sumBy { it.hp })
    }

    private fun printBoard(state: State) {
        /*for (y in 0 until state.height) {
            for (x in 0 until state.width) {
                val cell = state.board[Pair(x, y)]!!
                if(cell.isWall) {
                    print("#")
                } else if(cell.unit == null) {
                    print(".")
                } else {
                    if (cell.unit!!.isElf) {
                        print("E")
                    } else {
                        print("G")
                    }
                }
            }
            println()
        }


        state.elves.forEach {
            print("E:${it.hp} ")
        }
        state.goblins.forEach {
            print("G:${it.hp} ")
        }
        println()
        println()*/
    }

    private fun executeTurn(state: State, turnCell: Cell) {
        var newTurnCell = turnCell
        if (!hasEnemyNeighbour(state, turnCell, turnCell.unit!!.isElf)) {
            // Targets and in range
            val inRange = state.board.filter {
                !it.value.isWall && it.value.unit == null && hasEnemyNeighbour(state, it.value, turnCell.unit!!.isElf)
            }

            // Reachable

            // Nearest
            val nearestDistance = inRange.map {
                Math.abs(turnCell.x - it.value.x) + Math.abs(turnCell.y - it.value.y)
            }.min()

            val nearest = inRange.filter {
                nearestDistance == Math.abs(turnCell.x - it.value.x) + Math.abs(turnCell.y - it.value.y)
            }

            val mins = mutableListOf<Pair<Cell, Int>>()
            nearest.forEach {
                val cell = it.value

                if (!state.board[Pair(turnCell.x - 1, turnCell.y)]!!.isWall && state.board[Pair(turnCell.x - 1, turnCell.y)]!!.unit == null) {
                    mins.add(Pair(Cell(turnCell.x - 1, turnCell.y, false), getMinDistance(state.board, mutableSetOf(), cell, state.board[Pair(turnCell.x - 1, turnCell.y)]!!, Integer.MAX_VALUE, 0)))
                }
                if (!state.board[Pair(turnCell.x + 1, turnCell.y)]!!.isWall && state.board[Pair(turnCell.x + 1, turnCell.y)]!!.unit == null) {
                    mins.add(Pair(Cell(turnCell.x + 1, turnCell.y, false), getMinDistance(state.board, mutableSetOf(), cell, state.board[Pair(turnCell.x + 1, turnCell.y)]!!, Integer.MAX_VALUE, 0)))
                }
                if (!state.board[Pair(turnCell.x, turnCell.y - 1)]!!.isWall && state.board[Pair(turnCell.x, turnCell.y - 1)]!!.unit == null) {
                    mins.add(Pair(Cell(turnCell.x, turnCell.y - 1, false), getMinDistance(state.board, mutableSetOf(), cell, state.board[Pair(turnCell.x, turnCell.y - 1)]!!, Integer.MAX_VALUE, 0)))
                }
                if (!state.board[Pair(turnCell.x, turnCell.y + 1)]!!.isWall && state.board[Pair(turnCell.x, turnCell.y + 1)]!!.unit == null) {
                    mins.add(Pair(Cell(turnCell.x, turnCell.y + 1, false), getMinDistance(state.board, mutableSetOf(), cell, state.board[Pair(turnCell.x, turnCell.y + 1)]!!, Integer.MAX_VALUE, 0)))
                }
            }

            val destinationEntry = mins.minBy { it.second }
            if ( destinationEntry != null ) {
                val destination = destinationEntry.first
                if (destinationEntry.second != Integer.MAX_VALUE) {
                    state.board[Pair(destination.x, destination.y)]!!.unit = turnCell.unit
                    newTurnCell = state.board[Pair(destination.x, destination.y)]!!
                    turnCell.unit = null
                    newTurnCell.lastProcessed = state.round
                }
            }
        }

        // Attack
        var cellToAttack: Cell = Cell(0,0, true)
        var lowHp = Integer.MAX_VALUE

        val topCell = state.board[Pair(newTurnCell.x, newTurnCell.y - 1)]!!
        if (topCell.unit != null && topCell.unit!!.isElf != newTurnCell.unit!!.isElf) {
            if(topCell.unit!!.hp < lowHp) {
                cellToAttack = topCell
                lowHp = topCell.unit!!.hp
            }
        }

        val leftCell = state.board[Pair(newTurnCell.x - 1, newTurnCell.y)]!!
        if (leftCell.unit != null && leftCell.unit!!.isElf != newTurnCell.unit!!.isElf) {
            if(leftCell.unit!!.hp < lowHp) {
                cellToAttack = leftCell
                lowHp = leftCell.unit!!.hp
            }
        }

        val rightCell = state.board[Pair(newTurnCell.x + 1, newTurnCell.y)]!!
        if (rightCell.unit != null && rightCell.unit!!.isElf != newTurnCell.unit!!.isElf) {
            if(rightCell.unit!!.hp < lowHp) {
                cellToAttack = rightCell
                lowHp = rightCell.unit!!.hp
            }
        }

        val bottomCell = state.board[Pair(newTurnCell.x, newTurnCell.y + 1)]!!
        if (bottomCell.unit != null && bottomCell.unit!!.isElf != newTurnCell.unit!!.isElf) {
            if(bottomCell.unit!!.hp < lowHp) {
                cellToAttack = bottomCell
                lowHp = bottomCell.unit!!.hp
            }
        }

        if (!cellToAttack.isWall) {
            cellToAttack.unit!!.hp -= 3
            if (cellToAttack.unit!!.hp < 0) {
                state.elves.remove(cellToAttack.unit!!)
                state.goblins.remove(cellToAttack.unit!!)
                cellToAttack.unit = null
            }
        }


        /*val topCell = state.board[Pair(turnCell.x, turnCell.y - 1)]!!
        if (topCell.unit != null && topCell.unit!!.isElf != newTurnCell.unit!!.isElf) {
            topCell.unit!!.hp -= 3
            if (topCell.unit!!.hp < 0) {
                state.elves.remove(topCell.unit!!)
                state.goblins.remove(topCell.unit!!)
                topCell.unit = null
            }
            attackComplete = true
        }

        if (!attackComplete) {
            val leftCell = state.board[Pair(turnCell.x - 1, turnCell.y)]!!
            if (leftCell.unit != null && leftCell.unit!!.isElf != newTurnCell.unit!!.isElf) {
                leftCell.unit!!.hp -= 3
                if (leftCell.unit!!.hp < 0) {
                    state.elves.remove(leftCell.unit!!)
                    state.goblins.remove(leftCell.unit!!)
                    leftCell.unit = null
                }
                attackComplete = true
            }
        }

        if (!attackComplete) {
            val rightCell = state.board[Pair(turnCell.x + 1, turnCell.y)]!!
            if (rightCell.unit != null && rightCell.unit!!.isElf != newTurnCell.unit!!.isElf) {
                rightCell.unit!!.hp -= 3
                if (rightCell.unit!!.hp < 0) {
                    state.elves.remove(rightCell.unit!!)
                    state.goblins.remove(rightCell.unit!!)
                    rightCell.unit = null
                }
                attackComplete = true
            }
        }

        if (!attackComplete) {
            val bottomCell = state.board[Pair(turnCell.x, turnCell.y + 1)]!!
            if (bottomCell.unit != null && bottomCell.unit!!.isElf != newTurnCell.unit!!.isElf) {
                bottomCell.unit!!.hp -= 3
                if (bottomCell.unit!!.hp < 0) {
                    state.elves.remove(bottomCell.unit!!)
                    state.goblins.remove(bottomCell.unit!!)
                    bottomCell.unit = null
                }
            }
        }*/

    }

    fun testMinDistance(mapLayout: List<String>): Int {
        val state = createInitialState(mapLayout)
        val cell = state.board[Pair(3,3)]
        val distance = getMinDistance(state.board, mutableSetOf(), cell!!, state.board[Pair(1, 6)]!!, Integer.MAX_VALUE, 0)
        return distance
    }

    private fun getMinDistance(board: HashMap<Pair<Int,Int>, Cell>, visited: MutableSet<Pair<Int, Int>>, target: Cell, cell: Cell, minDist: Int, dist: Int): Int {
        if (cell.x == target.x && cell.y == target.y) {
            return Integer.min(minDist, dist)
        }

        visited.add(Pair(cell.x, cell.y))

        var newMinDist = Integer.MAX_VALUE
        if(cell.x - 1 == target.x && cell.y == target.y || (!visited.contains(Pair(cell.x - 1, cell.y)) && !board[Pair(cell.x - 1, cell.y)]!!.isWall && board[Pair(cell.x - 1, cell.y)]!!.unit == null)) {
            val tempMin = getMinDistance(board, visited, target, board[Pair(cell.x - 1, cell.y)]!!, minDist, dist + 1)
            if (tempMin < newMinDist) {
                newMinDist = tempMin
            }
        }
        if(cell.x + 1 == target.x && cell.y == target.y || (!visited.contains(Pair(cell.x + 1, cell.y)) && !board[Pair(cell.x + 1, cell.y)]!!.isWall && board[Pair(cell.x + 1, cell.y)]!!.unit == null)) {
            val tempMin = getMinDistance(board, visited, target, board[Pair(cell.x + 1, cell.y)]!!, minDist, dist + 1)
            if (tempMin < newMinDist) {
                newMinDist = tempMin
            }
        }
        if(cell.x == target.x && cell.y - 1 == target.y || (!visited.contains(Pair(cell.x, cell.y - 1)) && !board[Pair(cell.x, cell.y - 1)]!!.isWall && board[Pair(cell.x, cell.y - 1)]!!.unit == null)) {
            val tempMin = getMinDistance(board, visited, target, board[Pair(cell.x, cell.y - 1)]!!, minDist, dist + 1)
            if (tempMin < newMinDist) {
                newMinDist = tempMin
            }
        }
        if(cell.x == target.x && cell.y + 1 == target.y || (!visited.contains(Pair(cell.x, cell.y + 1)) && !board[Pair(cell.x, cell.y + 1)]!!.isWall && board[Pair(cell.x, cell.y + 1)]!!.unit == null)) {
            val tempMin = getMinDistance(board, visited, target, board[Pair(cell.x, cell.y + 1)]!!, minDist, dist + 1)
            if (tempMin < newMinDist) {
                newMinDist = tempMin
            }
        }

        visited.remove(Pair(cell.x, cell.y))

        return newMinDist
    }

    private fun hasEnemyNeighbour(state: State, cell: Cell, turnIsElf: Boolean): Boolean {

        val leftIsElf = state.board[Pair(cell.x - 1, cell.y)]?.unit?.isElf
        val left = leftIsElf != null && leftIsElf != turnIsElf && cell.x > 0
        val rightIsElf = state.board[Pair(cell.x + 1, cell.y)]?.unit?.isElf
        val right = rightIsElf != null && rightIsElf != turnIsElf && cell.x < state.width
        val topIsElf = state.board[Pair(cell.x, cell.y - 1)]?.unit?.isElf
        val top = topIsElf != null && topIsElf != turnIsElf && cell.y > 0
        val bottomIsElf = state.board[Pair(cell.x, cell.y + 1)]?.unit?.isElf
        val bottom = bottomIsElf != null && bottomIsElf != turnIsElf && cell.y < state.height

        return left || right || bottom || top
    }

    private fun createInitialState(mapLayout: List<String>): State {
        val board = LinkedHashMap<Pair<Int, Int>, Cell>()
        val elves = mutableListOf<Unit>()
        val goblins = mutableListOf<Unit>()

        mapLayout.forEachIndexed { y, row ->
            row.forEachIndexed { x, cell ->
                board[Pair(x, y)] = Cell(x, y, cell == '#')

                if (cell == 'E') {
                    board[Pair(x, y)]?.unit = Unit(true)
                    elves.add(board[Pair(x, y)]?.unit!!)
                } else if (cell == 'G') {
                    board[Pair(x, y)]?.unit = Unit(false)
                    goblins.add(board[Pair(x, y)]?.unit!!)
                }
            }
        }

        return State(board, mapLayout[0].length, mapLayout.size, elves, goblins)
    }

    private class State(val board: HashMap<Pair<Int, Int>, Cell>, val width: Int, val height: Int, val elves: MutableList<Unit>, val goblins: MutableList<Unit>) {
        var round = 0
    }

    private class Cell(val x: Int, val y: Int, val isWall: Boolean) {
        var unit: Unit? = null
        var lastProcessed = -1
    }

    private class Unit(val isElf: Boolean) {
        var hp = 200
    }
}