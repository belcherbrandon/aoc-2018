import java.util.*

object Day12 {

    private var offset: Long = 0

    fun runPart1(initialState: String, patternsList: List<String>): Long {
        offset = 0

        var pots = initialState
                .map {
                    it == '#'
                }

        val patterns = patternsList
                .map {
                    val split = it.split(" => ")
                    Pair(toBitSet(split[0]), split[1][0] == '#')
                }
                .toMap()

        // Mutation
        for (i in 0 until 20) {
            pots = nextGeneration(pots, patterns)

/*            pots.forEach {
                if (it) {
                    print("#")
                } else {
                    print(".")
                }
            }
            println()
            println("Generation: $i   Offset: $offset")*/
        }

        //offset = (50000000000 - 117) + 70

        // Count values
        return pots.foldIndexed(0L) { index, acc, it -> if (it) { acc + (index + offset) } else acc }
    }

    fun runPart2(initialState: String, patternsList: List<String>): Long {
        offset = 0

        var pots = initialState
                .map {
                    it == '#'
                }

        val patterns = patternsList
                .map {
                    val split = it.split(" => ")
                    Pair(toBitSet(split[0]), split[1][0] == '#')
                }
                .toMap()

        // Mutation
        var generation = 0
        var changed = true

        while (changed) {

            val nextGeneration = nextGeneration(pots, patterns)
            changed = pots != nextGeneration
            pots = nextGeneration

            pots.forEach {
                if (it) {
                    print("#")
                } else {
                    print(".")
                }
            }
            println()
            println("Generation: ${generation++}   Offset: $offset")
        }

        offset = 50000000000 - (generation - offset)

        // Count values
        return pots.foldIndexed(0L) { index, acc, it -> if (it) { acc + (index + offset) } else acc }
    }

    private fun nextGeneration(cur: List<Boolean>, patterns: Map<BitSet, Boolean>): List<Boolean> {
        val nextGeneration = MutableList(cur.size + 4) { false }

        for (i in 0 until nextGeneration.size) {
            val bitSet = BitSet(5)
            bitSet[0] = if (i >= 3 && i <= cur.size + 2) cur[i-3] else false
            bitSet[1] = if (i >= 2 && i <= cur.size + 1) cur[i-2] else false
            bitSet[2] = if (i >= 1 && i <= cur.size) cur[i-1] else false
            bitSet[3] = if (i <= cur.size - 1) cur[i] else false
            bitSet[4] = if (i <= cur.size - 2) cur[i+1] else false

            nextGeneration[i] = patterns.getOrDefault(bitSet, false)
        }

        val newStart = nextGeneration.indexOf(true)
        offset += (newStart - 1)

        return nextGeneration.subList(newStart, nextGeneration.lastIndexOf(true) + 1)
    }

    private fun toBitSet(input: String): BitSet {
        val bitSet = BitSet(5)

        for (i in 0 until 5) {
            bitSet[i] = input[i] == '#'
        }

        return bitSet
    }

}