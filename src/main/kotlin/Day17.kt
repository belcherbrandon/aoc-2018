object Day17 {

    fun runPart1(inputs: List<String>, example: Boolean): Int {
        val pair = runWater(inputs, example)
        return countWater(pair.first, pair.second)
    }

    fun runPart2(inputs: List<String>, example: Boolean): Int {
        val pair = runWater(inputs, example)
        return countStandingWater(pair.first, pair.second)
    }

    fun runWater(inputs: List<String>, example: Boolean): Pair<Array<Array<Char>>, Int> {
        val bounds = getBounds(inputs)
        val width = (bounds.first.second - bounds.first.first) + 7
        val height = bounds.second.second + 1
        val offset = bounds.first.first - 3

        // Populate board
        val board = Array(width) { Array(height) { '.' } }
        inputs.forEach {
            var xRange = 0..0
            var yRange = 0..0
            it.split(", ").forEach {
                val coord = it.split("=")
                if (coord[0] == "x") {
                    xRange = parseRange(coord[1])
                } else if (coord[0] == "y") {
                    yRange = parseRange(coord[1])
                }
            }

            for (y in yRange) {
                for (x in xRange) {
                    board[x - offset][y] = '#'
                }
            }
        }

        if (example) printBoard(board)

        fallAndFill(board, 500 - offset, 0, example)

        printBoard(board)

        return Pair(board, bounds.second.first)
    }

    private fun countWater(board: Array<Array<Char>>, top: Int): Int {
        var count = 0
        for (y in top until board[0].size) {
            for (x in 0 until board.size) {
                if (board[x][y] == '~' || board[x][y] == '|') {
                    count++
                }
            }
        }
        return count
    }

    private fun countStandingWater(board: Array<Array<Char>>, top: Int): Int {
        var count = 0
        for (y in top until board[0].size) {
            for (x in 0 until board.size) {
                if (board[x][y] == '~') {
                    count++
                }
            }
        }
        return count
    }

    private fun fallAndFill(board: Array<Array<Char>>, startX: Int, startY: Int, example: Boolean) {
        val x = startX
        var y = startY

        // Find bottom
        while (board[x][y + 1] != '#') {
            y++

            board[x][y] = '|'

            if (y + 1 >= board[0].size) {
                return
            }
        }

        // Fill layer
        var overflow = false

        while (!overflow) {

            val newRow = mutableListOf<Int>()

            board[x][y] = '|'

            // Fill left
            var tempX = x
            newRow.add(tempX)
            while (board[tempX - 1][y] != '#' && board[tempX - 1][y] != '~' && board[tempX][y + 1] != '.' && board[tempX][y + 1] != '|') {
                tempX--
                board[tempX][y] = '|'
                newRow.add(tempX)
            }
            if (board[tempX][y + 1] == '|') {
                overflow = true
            }
            if (board[tempX][y + 1] == '.') {
                overflow = true
                fallAndFill(board, tempX, y, example)
            }

            // Fill right
            tempX = x
            while (board[tempX + 1][y] != '#' && board[tempX + 1][y] != '~' && board[tempX][y + 1] != '.' && board[tempX][y + 1] != '|'){
                tempX++
                board[tempX][y] = '|'
                newRow.add(tempX)
            }
            if (board[tempX][y + 1] == '|') {
                overflow = true
            }
            if (board[tempX][y + 1] == '.') {
                overflow = true
                fallAndFill(board, tempX, y, example)
            }

            if (example) printBoard(board)

            //if (board[x][y] == '.') {
                //board[x][y] = '~'
            //}

            if (!overflow) {
                newRow.forEach{board[it][y] = '~'}
                y--
            }
        }
    }

    private fun printArea(board: Array<Array<Char>>, xRange: IntRange, yRange: IntRange) {
        for (y in yRange) {
            for (x in xRange) {
                print(board[x][y])
            }
            println()
        }
        println()
    }

    private fun printBoard(board: Array<Array<Char>>) {
        for (y in 0 until board[0].size) {
            for (x in 0 until board.size) {
                print(board[x][y])
            }
            println()
        }
        println()
    }

    private fun parseRange(rangeString: String): IntRange {
        val split = rangeString.split("..")
        return if(split.size > 1) {
            split[0].toInt() .. split[1].toInt()
        } else {
            split[0].toInt() .. split[0].toInt()
        }
    }

    private fun getBounds(inputs: List<String>): Pair<Pair<Int, Int>, Pair<Int, Int>> {
        var xBounds = Pair(Integer.MAX_VALUE, 0)
        var yBounds = Pair(Integer.MAX_VALUE, 0)

        inputs.map {
            it.split(", ").forEach {
                val coord = it.split("=")
                if (coord[0] == "x") {
                    xBounds = updateBounds(coord[1], xBounds)
                } else if (coord[0] == "y") {
                    yBounds = updateBounds(coord[1], yBounds)
                }

            }
        }

        return Pair(xBounds, yBounds)
    }

    private fun updateBounds(check: String, bounds: Pair<Int, Int>): Pair<Int, Int> {
        val split = check.split("..")
        var newBounds = updateBounds(split[0].toInt(), bounds)
        if(split.size > 1) {
            newBounds = updateBounds(split[1].toInt(), newBounds)
        }
        return newBounds
    }

    private fun updateBounds(check: Int, bounds: Pair<Int, Int>): Pair<Int, Int> {
        var min = bounds.first
        var max = bounds.second
        if (check < min) {
            min = check
        }
        if (check > max) {
            max = check
        }
        return Pair(min, max)
    }

}