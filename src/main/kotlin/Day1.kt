object Day1 {

    fun runPart1(input: IntArray) = input.sum()

    fun runPart2(input: IntArray): Int {
        generateSequence(0) { it + 1 }
            .fold(Accumulator()) { visited, it ->
                val newSum = visited.sum + input[it % input.size]

                val exists = !visited.add(newSum)
                if (exists) return newSum

                visited.sum = newSum
                visited
            }

        return 0 // Unreachable
    }

    private class Accumulator {
        private val set = mutableSetOf(0)
        fun add(element: Int): Boolean = set.add(element)
        var sum = 0
    }
}