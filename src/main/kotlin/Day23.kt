object Day23 {

    fun runPart1(inputs: List<String>): Int {
        val bots = inputs.map {
            val split = it.split("pos=<", ",", ">, r=")
            Bot(split[1].toInt(), split[2].toInt(), split[3].toInt(), split[4].toInt())
        }

        val bigBot = bots.maxWith(Comparator { a, b -> a.r.compareTo(b.r) })

        return if (bigBot != null) {
            bots.filter { distance(it.x, it.y, it.z, bigBot) <= bigBot.r }.count()
        } else {
            0
        }
    }

    fun runPart2(inputs: List<String>): Int {
        val bots = inputs.map {
            val split = it.split("pos=<", ",", ">, r=")
            Bot(split[1].toInt(), split[2].toInt(), split[3].toInt(), split[4].toInt())
        }

        val botsZ1000 = bots.map { Bot(it.x / 1000, it.y / 1000, it.z / 1000, it.r / 1000) }

        var minX = Integer.MAX_VALUE
        var maxX = Integer.MIN_VALUE
        var minY = Integer.MAX_VALUE
        var maxY = Integer.MIN_VALUE
        var minZ = Integer.MAX_VALUE
        var maxZ = Integer.MIN_VALUE

        botsZ1000.forEach {
            if(it.x < minX) {
                minX = it.x
            }
            if(it.x > maxX) {
                maxX = it.x
            }
            if(it.y < minY) {
                minY = it.y
            }
            if(it.y > maxY) {
                maxY = it.y
            }
            if(it.z < minZ) {
                minZ = it.z
            }
            if(it.z > maxZ) {
                maxZ = it.z
            }
        }

        val coord1000 = checkWithZoom(minX, maxX, minY, maxY, minZ, maxZ, botsZ1000)

        val botsZ100 = bots.map { Bot(it.x / 100, it.y / 100, it.z / 100, it.r / 100) }

        val coord100 = checkWithZoom(coord1000.first * 10, coord1000.first * 10, minY, maxY, minZ, maxZ, botsZ1000)


        //val size = (maxX - minX) * (maxY - minY) * (maxZ - minZ)
        //size

        val botsInRange = mutableMapOf<Triple<Int, Int, Int>, Int>()
        for (x in minX .. maxX) {
            for (y in minY .. maxY) {
                for (z in minZ .. maxZ) {
                    val numBotsInRange = bots.filter {
                        distance(x, y, z, it) <= it.r
                    }.count()
                    if (numBotsInRange > 1) {
                        botsInRange[Triple(x, y, z)] = numBotsInRange
                    }
                }
            }
        }

        val maxCount = botsInRange.values.max()

        val maxes = botsInRange.filter { it.value == maxCount }

        // Tiebreaker
        val maxCoord = maxes.entries.first()

        return 0

    }

    private fun checkWithZoom(minX: Int, maxX: Int, minY: Int, maxY: Int, minZ: Int, maxZ: Int, bots: List<Bot>): Triple<Int, Int, Int> {
        val botsInRange = mutableMapOf<Triple<Int, Int, Int>, Int>()
        for (x in minX .. maxX) {
            for (y in minY .. maxY) {
                for (z in minZ .. maxZ) {
                    val numBotsInRange = bots.filter {
                        distance(x, y, z, it) <= it.r
                    }.count()
                    if (numBotsInRange > 1) {
                        botsInRange[Triple(x, y, z)] = numBotsInRange
                    }
                }
            }
        }

        val maxCount = botsInRange.values.max()

        val maxes = botsInRange.filter { it.value == maxCount }

        // Tiebreaker
        val maxCoord = maxes.entries.first()

        return maxCoord.key
    }

    private fun distance(x: Int, y: Int, z: Int, bot: Bot) = Math.abs(x - bot.x) + Math.abs(y - bot.y) + Math.abs(z - bot.z)

    class Bot(val x: Int, val y: Int, val z: Int, val r: Int)
}