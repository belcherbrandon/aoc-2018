object Day5 {

    fun runPart1(rawInput: String): Int {
        val input: MutableList<Char> = "${rawInput}_".toMutableList()
        var i = 0
        while (i < input.size - 1) {
            if (input[i] != input[i+1] && input[i].equals(input[i+1], true)) {
                input.removeAt(i)
                input.removeAt(i)
                if (i > 0) i--
            } else {
                i++
            }
        }

        return input.size - 1
    }

    fun runPart2(input: String): Int {
        var bestLength = input.length
        for(i in 'a'..'z') {
            val length = runPart1(input.replace(i.toString(), "", true))
            if (length < bestLength) bestLength = length
        }
        return bestLength
    }

}