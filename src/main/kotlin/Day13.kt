object Day13 {

    fun runPart1(trackLayout: List<String>): String {

        val tracks = buildTrack(trackLayout).first
        var time = 0

        while (true) {
            tracks.forEach {
                val cart = it.value.cart
                if (cart != null && cart.lastMoved != time) {

                    if (it.value.type == '+') {
                        if (cart.cycle % 3 == 0) {
                            cart.direction = cart.direction.left()
                        } else if (cart.cycle % 3 == 2) {
                            cart.direction = cart.direction.right()
                        }
                        cart.cycle++
                    } else if ((it.value.type == '/' && (cart.direction == Cardinal.N || cart.direction == Cardinal.S)) ||
                            (it.value.type == '\\' && (cart.direction == Cardinal.E || cart.direction == Cardinal.W))) {
                        cart.direction = cart.direction.right()
                    } else if ((it.value.type == '/' && (cart.direction == Cardinal.E || cart.direction == Cardinal.W)) ||
                            (it.value.type == '\\' && (cart.direction == Cardinal.N || cart.direction == Cardinal.S))) {
                        cart.direction = cart.direction.left()
                    }

                    if (cart.direction == Cardinal.E) {
                        if (!move(it.value, it.value.e!!)) {
                            return "${it.key.first + 1},${it.key.second}"
                        }
                    } else if (cart.direction == Cardinal.W) {
                        if (!move(it.value, it.value.w!!)) {
                            return "${it.key.first - 1},${it.key.second}"
                        }
                    } else if (cart.direction == Cardinal.N) {
                        if (!move(it.value, it.value.n!!)) {
                            return "${it.key.first},${it.key.second - 1}"
                        }
                    } else if (cart.direction == Cardinal.S) {
                        if (!move(it.value, it.value.s!!)) {
                            return "${it.key.first},${it.key.second + 1}"
                        }
                    }
                    cart.lastMoved = time
                }
            }
            //printTrack(trackLayout, tracks)
            time++
        }
    }

    private fun printTrack(trackLayout: List<String>, tracks: LinkedHashMap<Pair<Int, Int>, Day13.Track>) {
        trackLayout.forEachIndexed { y, row ->
            row.forEachIndexed { x, cell ->
                if (tracks.contains(Pair(x, y))) {
                    if(tracks[Pair(x, y)]?.cart != null) {
                        print(tracks[Pair(x, y)]?.cart?.direction)
                    } else {
                        print(tracks[Pair(x, y)]?.type)
                    }
                } else {
                    print(" ")
                }
            }
            println()
        }
    }

    fun runPart2(trackLayout: List<String>): String {
        val pair = buildTrack(trackLayout)
        val tracks = pair.first
        var numCarts = pair.second
        var time = 0

        while (numCarts > 1) {
            tracks.forEach {
                val cart = it.value.cart
                if (cart != null && cart.lastMoved != time) {

                    if (it.value.type == '+') {
                        if (cart.cycle % 3 == 0) {
                            cart.direction = cart.direction.left()
                        }
                        if (cart.cycle % 3 == 2) {
                            cart.direction = cart.direction.right()
                        }
                        cart.cycle++

                    } else if ((it.value.type == '/' && (cart.direction == Cardinal.N || cart.direction == Cardinal.S)) ||
                            (it.value.type == '\\' && (cart.direction == Cardinal.E || cart.direction == Cardinal.W))) {
                        cart.direction = cart.direction.right()
                    } else if ((it.value.type == '/' && (cart.direction == Cardinal.E || cart.direction == Cardinal.W)) ||
                            (it.value.type == '\\' && (cart.direction == Cardinal.N || cart.direction == Cardinal.S))) {
                        cart.direction = cart.direction.left()
                    }

                    if (cart.direction == Cardinal.E) {
                        if (!move(it.value, it.value.e!!)) {
                            it.value.cart = null
                            it.value.e?.cart = null
                            numCarts -= 2
                        }
                    } else if (cart.direction == Cardinal.W) {
                        if (!move(it.value, it.value.w!!)) {
                            it.value.cart = null
                            it.value.w?.cart = null
                            numCarts -= 2
                        }
                    } else if (cart.direction == Cardinal.N) {
                        if (!move(it.value, it.value.n!!)) {
                            it.value.cart = null
                            it.value.n?.cart = null
                            numCarts -= 2
                        }
                    } else if (cart.direction == Cardinal.S) {
                        if (!move(it.value, it.value.s!!)) {
                            it.value.cart = null
                            it.value.s?.cart = null
                            numCarts -= 2
                        }
                    }
                    cart.lastMoved = time
                }
            }
            time++
        }

        tracks.forEach {
            val cart = it.value.cart
            if (cart != null) {
                return "${it.key.first},${it.key.second}"
            }
        }

        return ""
    }

    private fun buildTrack(trackLayout: List<String>): Pair<LinkedHashMap<Pair<Int, Int>, Track>,Int> {
        val tracks = linkedMapOf<Pair<Int, Int>, Track>()
        var numCarts = 0

        trackLayout.forEachIndexed { y, row ->
            row.forEachIndexed { x, cell ->
                if (cell != ' ') {

                    var type = cell
                    if (cell == 'v' || cell == '^') {
                        type = '|'
                    } else if (cell == '>' || cell == '<') {
                        type = '-'
                    }

                    val newTrack = Track(type)

                    if (cell == '|' || cell == '^' || cell == 'v' || cell == '+' ||
                            ((cell == '\\' || cell == '/') && y > 0 && (tracks[Pair(x, y - 1)]?.hasSoutherlyConnection() == true))) {
                        val existing = tracks[Pair(x, y - 1)]
                        newTrack.n = existing
                        existing?.s = newTrack
                    }

                    if (cell == '-' || cell == '<' || cell == '>' || cell == '+' ||
                            ((cell == '\\' || cell == '/') && x > 0 && (tracks[Pair(x - 1, y)]?.hasEasterlyConnection() == true))) {
                        val existing = tracks[Pair(x - 1, y)]
                        newTrack.w = existing
                        existing?.e = newTrack
                    }

                    if (cell == 'v' || cell == '^' || cell == '<' || cell == '>') {
                        newTrack.cart = Cart(cell)
                        numCarts++
                    }

                    tracks[Pair(x, y)] = newTrack
                }
            }
        }

        return Pair(tracks, numCarts)
    }

    private fun move(source: Track, destination: Track): Boolean =
            if (destination.cart != null) {
                false
            } else {
                destination.cart = source.cart
                source.cart = null
                true
            }

    private class Track(val type: Char) {
        var n: Track? = null
        var s: Track? = null
        var e: Track? = null
        var w: Track? = null
        var cart: Cart? = null

        fun hasSoutherlyConnection() = type == '|' || type == '/' || type == '\\' || type == '+' || type == 'v' || type == '^'
        fun hasEasterlyConnection()  = type == '-' || type == '/' || type == '\\' || type == '+' || type == '<' || type == '>'
    }

    class Cart(cardinal: Char) {
        var direction = Cardinal.fromChar(cardinal)
        var cycle = 0
        var lastMoved = -1
    }

    enum class Cardinal(val symbol: Char) {
        N('^'),
        E('>'),
        S('v'),
        W('<');

        fun right(): Cardinal {
            val values= values()
            val nextOrdinal = (ordinal + 1) % values.size
            return values[nextOrdinal]
        }

        fun left(): Cardinal {
            val values= values()
            val nextOrdinal = (ordinal + values.size - 1) % values.size
            return values[nextOrdinal]
        }

        companion object {
            private val map = Cardinal.values().associateBy(Cardinal::symbol)
            fun fromChar(symbol: Char) = map[symbol] ?: N
        }
    }
}