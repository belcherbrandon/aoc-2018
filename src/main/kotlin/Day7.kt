object Day7 {

    fun runPart1(input: List<String>): String {

        val graph = buildGraph(input)

        val start = Node('_')
        start.complete = true
        start.to.addAll(graph.values.filter{ it.from.size == 0 })

        val visited = ArrayList<Char>()
        traverseGraph(visited, mutableSetOf(), start)

        return visited.fold(String()) {
            acc, c -> acc + c
        }.drop(1)
    }

    private fun traverseGraph(visited: ArrayList<Char>, options: MutableSet<Node>, node: Node) {
        node.complete = true
        visited.add(node.id)
        options.remove(node)

        options.addAll(node.to)
        val completes = options.filter { option -> option.from.all { prereq -> prereq.complete } }
        if (completes.isEmpty()) {
            return
        }

        val choice = completes.sortedBy { it.id }.first()

        traverseGraph(visited, options, choice)
    }

    private fun buildGraph(input: List<String>): HashMap<Char, Node> =
        input.fold(HashMap<Char, Node>()) { acc, it ->
            val split = it.split(" ")
            val prereqId = split[1][0]
            val id = split[7][0]

            val node = acc.getOrPut(id) { Node(id) }
            val prereq = acc.getOrPut(prereqId) { Node(prereqId) }
            node.from.add(prereq)
            prereq.to.add(node)

            acc
        }

    class Node(val id: Char) {
        val from = ArrayList<Node>()
        val to = ArrayList<Node>()
        var complete = false
        var timeFinished = 0
    }

    fun runPart2(input: List<String>, maxWorkers: Int, baseEffort: Int): Int {

        val graph = buildGraph(input)
        var time = 0

        val working = mutableListOf<Node>()
        working.addAll(graph.values.filter{ it.from.size == 0 }.take(maxWorkers))
        working.forEach {
            it.timeFinished = time + getNodeEffort(it.id) + baseEffort
        }

        val options = mutableSetOf<Node>()

        while (true) {
            // Check for finished nodes
            val newlyCompleted = working.filter { it.timeFinished == time }

            if (newlyCompleted.isNotEmpty()) {
                newlyCompleted.forEach {
                    working.remove(it)
                    it.complete = true

                    it.to.forEach { potentialOption ->
                        if (!working.contains(potentialOption) && !potentialOption.complete) {
                            options.add(potentialOption)
                        }
                    }
                    println("$time: ${it.id}")
                }

                // Give more nodes to workers
                if (working.size < maxWorkers) {
                    val completes = options.filter { option -> option.from.all { prereq -> prereq.complete } }
                    val sortedChoices = completes.sortedBy { it.id }
                    val addedNodes = sortedChoices.take(maxWorkers - working.size)
                    addedNodes.forEach {
                        it.timeFinished = time + getNodeEffort(it.id) + baseEffort
                        options.remove(it)
                    }
                    working.addAll(addedNodes)
                }

                if (working.size == 0) {
                    return time
                }
            }

            time++
        }
    }

    fun getNodeEffort(id: Char): Int = id.toInt() - 64
}