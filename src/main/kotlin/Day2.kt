object Day2 {

    fun runPart1(input: List<String>) : Int {
        val finalCounts = input
                .fold(Pair(0, 0)) { acc, line ->
                    val counts = line.groupingBy { it }.eachCount()
                    Pair(acc.first + counts.containsValue(2).toInt(), acc.second + counts.containsValue(3).toInt())
                }

        // Multiply the number of lines with doubles by the number of lines with triples
        return finalCounts.first * finalCounts.second
    }

    fun runPart2(input: List<String>) : String {
        // Create a list of type Code which has an overridden equals()
        val codes = input.map { Code(it) }.toMutableList()
        codes.retainAll(codes)
        return getCommonLetters(codes[0].value, codes[1].value)
    }

    private fun getCommonLetters(code1: String, code2: String): String {
        return code1.filterIndexed { index, _ ->  code1[index] == code2[index] }
    }

    class Code(val value: String) {

        override fun hashCode() = value.hashCode()

        /**
         * Considered equal if values differ by one and only one character
         */
        override fun equals(other: Any?) = other?.let {
            if (value == (other as Code).value) {
                return false
            }

            var oneMiss = false
            value.forEachIndexed { index, c ->
                if(c != (it as Code).value[index]) {
                    if (oneMiss) {
                        oneMiss = false
                        return@equals false
                    }
                    oneMiss = true
                }
            }
            oneMiss
        } ?: false

    }

    private fun Boolean.toInt() = if (this) 1 else 0
}
