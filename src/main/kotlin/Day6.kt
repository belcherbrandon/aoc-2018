object Day6 {

    fun runPart1(input: List<String>): Int {
        val sites = getSites(input)
        val board = generateBoard(sites)

        board.forEach { coord ->
            val sortedByDistances = sites.sortedBy { site -> coord.distanceBetween(site) }
            if (coord.distanceBetween(sortedByDistances[0]) != coord.distanceBetween(sortedByDistances[1])) {
                coord.closestSite = sortedByDistances[0]
            }
        }

        return board
            .groupingBy { it.closestSite }
            .eachCount()
            .maxBy { it.value }?.value ?: 0
    }

    fun runPart2(input: List<String>, limit: Int): Int {
        val sites = getSites(input)
        val board = generateBoard(sites)

        return board.map { coord ->
            val sum = sites
                .sumBy {site ->
                    coord.distanceBetween(site)
                }
            if (sum < limit) 1 else 0
        }.sum()
    }

    private fun getSites(input: List<String>) = input.map {
        val split = it.split(", ")
        Pair(split[0].toInt(), split[1].toInt())
    }

    private fun generateBoard(sites: List<Pair<Int, Int>>): List<Coord> {
        val xMin = sites.minBy { it.first }!!.first
        val xMax = sites.maxBy { it.first }!!.first
        val yMin = sites.minBy { it.second }!!.second
        val yMax = sites.maxBy { it.second }!!.second

        val board = mutableListOf<Coord>()
        for (i in xMin..xMax) {
            for (j in yMin..yMax) {
                board.add(Coord(i, j))
            }
        }
        return board
    }

    private class Coord(val x: Int, val y: Int) {
        var closestSite: Pair<Int, Int> = Pair(-1, -1)

        fun distanceBetween(site: Pair<Int, Int>): Int {
            return Math.abs(x - site.first) + Math.abs(y - site.second)
        }
    }
}