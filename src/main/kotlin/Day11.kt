object Day11 {

    fun runPart1(serial: Int, range: IntRange): String {
        val size = 300

        var highestPower = 0
        var highestCoord = ""

        val board = Array(size) { Array(size) { 0 } }

        for (i in 0 until size) {
            for (j in 0 until size) {
                board[i][j] = calculatePowerLevel(i, j, serial)
            }
        }

        for (neighbourSize in range) {
            for (i in 0 until size) {
                for (j in 0 until size) {
                    if (i + neighbourSize < size && j + neighbourSize < size) {
                        val neighboursPower = calculateNeighboursPower(board, neighbourSize, i, j)
                        if (neighboursPower > highestPower) {
                            highestPower = neighboursPower
                            highestCoord = "$i,$j,$neighbourSize"
                        }
                    }
                }
            }
        }

        return highestCoord
    }

    fun calculatePowerLevel(x: Int, y: Int, serial: Int): Int {
        val rackId = x + 10
        return ((rackId * y + serial) * rackId / 100) % 10 - 5
    }

    fun calculateNeighboursPower(board: Array<Array<Int>>, neighbourSize: Int, x: Int, y: Int): Int {
        var sum = 0
        for (i in x until Math.min(x + neighbourSize, board.size)) {
            for (j in y until Math.min(y + neighbourSize, board.size)) {
                sum += board[i][j]
            }
        }
        return sum
    }

}