object Day9 {

    fun runPart1(numPlayers: Int, finalMarble: Int): Long {

        val scores = mutableMapOf<Int, Long>()
        var currentPlayer = 1
        var currentMarbleNumber = 1
        val firstMarble = Marble(0)
        var marble = firstMarble

        while (currentMarbleNumber <= finalMarble) {
            if (currentMarbleNumber % 23 == 0) {
                marble = marble.left.left.left.left.left.left.left
                val removedMarbleNumber = marble.value
                marble = removeMarble(marble)
                scores[currentPlayer] = scores.getOrDefault(currentPlayer, 0) + currentMarbleNumber + removedMarbleNumber
            } else {
                marble = insertMarble(marble.right.right, currentMarbleNumber)
            }

            //printBoard(firstMarble, currentMarbleNumber, currentPlayer)

            currentMarbleNumber++
            currentPlayer = ++currentPlayer % numPlayers
        }

        return scores.maxBy { it.value }!!.value
    }

    // Remove and return marble that took its position
    private fun removeMarble(marble: Marble): Marble {
        val marbleLeft = marble.left
        val marbleRight = marble.right
        marbleLeft.right = marbleRight
        marbleRight.left = marbleLeft
        return marbleRight
    }

    fun insertMarble(currentMarble: Marble, marbleNumber: Int): Marble {
        val newMarble = Marble(marbleNumber, currentMarble.left, currentMarble)
        currentMarble.left.right = newMarble
        currentMarble.left = newMarble
        return newMarble
    }

    class Marble() {

        var value: Int = 0
        lateinit var left: Marble
        lateinit var right: Marble

        constructor(value: Int) : this() {
            this.value = value
            left = this
            right = this
        }

        constructor(value: Int, left: Marble, right: Marble) : this() {
            this.value = value
            this.left = left
            this.right = right
        }

    }

    fun printBoard(marble: Marble, currentMarbleNumber: Int, currentPlayer: Int) {
        print("[$currentPlayer] ")

        print("${marble.value} ")

        var currentMarble = marble.right

        while (currentMarble != marble) {
            if (currentMarble.value == currentMarbleNumber) {
                print("(${currentMarble.value}) ")
            } else {
                print("${currentMarble.value} ")
            }
            currentMarble = currentMarble.right
        }

        println()
    }

}