object Day4 {

    fun runPart1(input: List<String>): Int {
        val schedule = Array(60) { ArrayList<Int>() }
        populateSchedule(schedule, input)

        val sleepiestGuard = findSleepiestGuard(schedule)
        val sleepiestMinute = findGuardsSleepiestMinute(schedule, sleepiestGuard)

        return sleepiestGuard * sleepiestMinute
    }

    private fun populateSchedule(schedule: Array<ArrayList<Int>>, input: List<String>) {
        var guard = 0
        var startTime = 0

        input
            .sorted()
            .forEach {
                val splitLine = it.split("] ")
                val minute = splitLine[0].split(":")[1].toInt()
                val message = splitLine[1]

                when {
                    message.startsWith("Guard") -> guard = message.split(" #", " ")[1].toInt()
                    message.startsWith("falls") -> startTime = minute
                    message.startsWith("wakes") -> recordTimes(schedule, guard, startTime, minute)
                }
            }
    }

    private fun recordTimes(schedule: Array<ArrayList<Int>>, guard: Int, startTime: Int, endTime: Int) {
        for (i in startTime until endTime) {
            schedule[i].add(guard)
        }
    }

    private fun findSleepiestGuard(schedule: Array<ArrayList<Int>>): Int {
        return schedule
                .flatMap {it}
                .groupingBy {it}
                .eachCount()
                .maxBy { it.value }
                ?.key ?: 0
    }

    private fun findGuardsSleepiestMinute(schedule: Array<ArrayList<Int>>, guard: Int): Int {
        val counts = schedule.map { minute -> minute.filter { it == guard }.count() }
        return counts.indexOf(counts.max())
    }

    fun runPart2(input: List<String>): Int {
        val schedule = Array(60) { ArrayList<Int>() }
        populateSchedule(schedule, input)

        val max = schedule
                .mapIndexed { index, minute ->
                    val minuteMax = minute
                            .groupingBy { it }
                            .eachCount()
                            .maxBy { it.value }
                    Triple(index, minuteMax?.key ?: 0, minuteMax?.value ?: 0)
                }
                .maxBy { it.third }

        return (max?.first ?: 0) * (max?.second ?:0)
    }
}