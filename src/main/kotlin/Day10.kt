object Day10 {

    fun runPart1(input: List<String>, frame: Int) {
        var minX = 100000
        var maxX = -100000
        var minY = 100000
        var maxY = -100000

        input.forEach {
            val split = it.replace("\\s".toRegex(), "").split("<", ",", ">")
            val initialX  = split[1].toInt()
            val initialY  = split[2].toInt()
            val velocityX = split[4].toInt() * frame
            val velocityY = split[5].toInt() * frame
            val newX = initialX + velocityX
            val newY = initialY + velocityY

            if (newX > maxX) maxX = newX
            if (newX < minX) minX = newX
            if (newY > maxY) maxY = newY
            if (newY < minY) minY = newY
        }

        val zoom =
            if (maxX - minX > 10000 || maxY - minY > 10000) {
                1000
            } else if (maxX - minX > 1000 || maxY - minY > 1000) {
                100
            }  else if (maxX - minX > 100 || maxY - minY > 100) {
                10
            } else {
                1
            }


        val xCorrection = minX * -1 + 5
        val yCorrection = minY * -1 + 5
        val board = Array((maxX - minX) / zoom + 10) { Array((maxY - minY) / zoom + 10) { false } }

        input.forEach {
            val split = it.replace("\\s".toRegex(), "").split("<", ",", ">")
            val initialX  = split[1].toInt()
            val initialY  = split[2].toInt()
            val velocityX = split[4].toInt() * frame
            val velocityY = split[5].toInt() * frame
            val newX = initialX + velocityX
            val newY = initialY + velocityY

            board[(newX + xCorrection) / zoom][(newY + yCorrection) / zoom] = true
        }

        if (zoom == 1) {
            printBoard(board)
        }
    }

    private fun printBoard(board: Array<Array<Boolean>>) {
        for (i in 0 until board.size) {
            for (j in 0 until board[i].size) {
                if (board[i][j]) {
                    print(".")
                } else {
                    print("#")
                }
            }
            println()
        }

        println()
    }
}