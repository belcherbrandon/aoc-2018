object Day14 {

    fun runPart1(recipeCount: Int): String {
        val recipes = createRecipes(recipeCount)
        return recipes.take(recipeCount + 10).takeLast(10).joinToString("")
    }

    fun runPart2(recipeCount: Int, searchString: String): Int {
        val recipes = createRecipes(recipeCount)

        for (i in 0 .. recipes.size - 6) {
            var curString = ""
            for(j in 0 until searchString.length) {
                curString += recipes[i + j]
            }
            if (curString == searchString) {
                return i
            }
        }

        return -1
    }

    private fun createRecipes(recipeCount: Int): List<Int> {
        val recipes = mutableListOf<Int>(3, 7)
        var index1 = 0
        var index2 = 1

        while (recipes.size < recipeCount + 10) {
            val score1 = recipes[index1]
            val score2 = recipes[index2]

            // Add new recipes
            (score1 + score2).toString().forEach {
                recipes.add(it.toInt() - 48)
            }

            // Move elves on
            index1 = (index1 + score1 + 1) % recipes.size
            index2 = (index2 + score2 + 1) % recipes.size
        }
        return recipes
    }
}