import java.util.ArrayDeque

object Day8 {

    class Header(var childrenToProcess: Int, val numMeta: Int)

    fun runPart1(input: IntArray): Int {
        val stack = ArrayDeque<Header>()
        stack.push(Header(1,0))

        var index = 0
        var sum = 0

        while (index < input.size) {
            if (stack.peek().childrenToProcess == 0) {
                val header = stack.pop()
                sum += input.slice(index until index + header.numMeta).sum()
                index += header.numMeta
                stack.peek().childrenToProcess--
            } else {
                stack.push(Header(input[index++], input[index++]))
            }
        }

        return sum
    }

    class Node(val isLeaf: Boolean, var childrenToProcess: Int, val numMeta: Int) {
        var value: Int = 0
        val children = mutableListOf<Node>()
    }

    fun runPart2(input: IntArray): Int {
        val rootNode = Node(false, 1, 0)
        var parentNode = rootNode

        val stack = ArrayDeque<Node>()
        stack.push(rootNode)

        var index = 0

        while (index < input.size) {
            if (stack.peek().childrenToProcess == 0) {
                val header = stack.pop()
                parentNode = stack.peek()
                if (header.isLeaf) {
                    header.value = input.slice(index until index + header.numMeta).sum()
                } else {
                    header.value = input
                            .slice(index until index + header.numMeta)
                            .fold(0) { acc, it ->
                                if (it - 1 < header.children.size) {
                                    acc + header.children[it - 1].value
                                } else {
                                    acc
                                }
                            }
                }

                index += header.numMeta
                stack.peek().childrenToProcess--
            } else {
                val numChildren = input[index++]
                val numMeta = input[index++]

                val node = Node(numChildren == 0, numChildren, numMeta)
                parentNode.children.add(node)
                parentNode = node
                stack.push(node)
            }
        }

        return rootNode.children[0].value
    }
}