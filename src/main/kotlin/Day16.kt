object Day16 {

    fun runPart1(inputs: List<String>): Int {
        val windows = inputs.windowed(4, 4)

        var tally = mutableListOf<Pair<Int, String>>()


        val count = windows.filter {
            var potential = 0
            val input: IntArray = parseRegisters(it[0])
            val instruction = parseInstruction(it[1])
            val output = parseRegisters(it[2])


            if (addr(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "addr"))
            }
            if (addi(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "addi"))
            }
            if (mulr(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "mulr"))
            }
            if (muli(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "muli"))
            }
            if (banr(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "banr"))
            }
            if (bani(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "bani"))
            }
            if (borr(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "borr"))
            }
            if (bori(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "bori"))
            }
            if (setr(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "setr"))
            }
            if (seti(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "seti"))
            }
            if (gtir(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "gtir"))
            }
            if (gtri(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "gtri"))
            }
            if (gtrr(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "gtrr"))
            }
            if (eqir(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "eqir"))
            }
            if (eqri(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "eqri"))
            }
            if (eqrr(input, instruction).contentEquals(output)) {
                potential++
                tally.add(Pair(instruction[0], "eqrr"))
            }

            potential >= 3
        }.size

        tally = tally.filter {
            it.first != 0 && it.second != "gtri" &&
            it.first != 1 && it.second != "bani" &&
            it.first != 2 && it.second != "eqrr" &&
            it.first != 3 && it.second != "gtir" &&
            it.first != 4 && it.second != "eqir" &&
            it.first != 5 && it.second != "bori" &&
            it.first != 6 && it.second != "seti" &&
            it.first != 7 && it.second != "setr" &&
            it.first != 8 && it.second != "addr" &&
            it.first != 9 && it.second != "borr" &&
            it.first != 10 && it.second != "muli" &&
            it.first != 11 && it.second != "banr" &&
            it.first != 12 && it.second != "addi" &&
            it.first != 13 && it.second != "eqri" &&
            it.first != 14 && it.second != "mulr" &&
            it.first != 15 && it.second != "gtrr"
        }.toMutableList()

        val addrCounts = tally.filter {it.second == "addr"}.groupingBy { it.first }.eachCount()
        val addiCounts = tally.filter {it.second == "addi"}.groupingBy { it.first }.eachCount()
        val mulrCounts = tally.filter {it.second == "mulr"}.groupingBy { it.first }.eachCount()
        val muliCounts = tally.filter {it.second == "muli"}.groupingBy { it.first }.eachCount()
        val banrCounts = tally.filter {it.second == "banr"}.groupingBy { it.first }.eachCount()
        val baniCounts = tally.filter {it.second == "bani"}.groupingBy { it.first }.eachCount()
        val borrCounts = tally.filter {it.second == "borr"}.groupingBy { it.first }.eachCount()
        val boriCounts = tally.filter {it.second == "bori"}.groupingBy { it.first }.eachCount()
        val setrCounts = tally.filter {it.second == "setr"}.groupingBy { it.first }.eachCount()
        val setiCounts = tally.filter {it.second == "seti"}.groupingBy { it.first }.eachCount()

        val gtirCounts = tally.filter {it.second == "gtir"}.groupingBy { it.first }.eachCount()
        val gtriCounts = tally.filter {it.second == "gtri"}.groupingBy { it.first }.eachCount()
        val gtrrCounts = tally.filter {it.second == "gtrr"}.groupingBy { it.first }.eachCount()

        val eqirCounts = tally.filter {it.second == "eqir"}.groupingBy { it.first }.eachCount()
        val eqriCounts = tally.filter {it.second == "eqri"}.groupingBy { it.first }.eachCount()
        val eqrrCounts = tally.filter {it.second == "eqrr"}.groupingBy { it.first }.eachCount()

        return count
    }

    fun runPart2(inputs: List<String>): Int {
        var registers = intArrayOf(0,0,0,0)
        inputs.forEach {
            val instruction = parseInstruction(it)
            when {
                instruction[0] == 0  -> registers = gtri(registers, instruction)
                instruction[0] == 1  -> registers = bani(registers, instruction)
                instruction[0] == 2  -> registers = eqrr(registers, instruction)
                instruction[0] == 3  -> registers = gtir(registers, instruction)
                instruction[0] == 4  -> registers = eqir(registers, instruction)
                instruction[0] == 5  -> registers = bori(registers, instruction)
                instruction[0] == 6  -> registers = seti(registers, instruction)
                instruction[0] == 7  -> registers = setr(registers, instruction)
                instruction[0] == 8  -> registers = addr(registers, instruction)
                instruction[0] == 9  -> registers = borr(registers, instruction)
                instruction[0] == 10 -> registers = muli(registers, instruction)
                instruction[0] == 11  -> registers = banr(registers, instruction)
                instruction[0] == 12  -> registers = addi(registers, instruction)
                instruction[0] == 13  -> registers = eqri(registers, instruction)
                instruction[0] == 14  -> registers = mulr(registers, instruction)
                instruction[0] == 15  -> registers = gtrr(registers, instruction)
            }
        }
        return registers[0]
    }

    private fun parseRegisters(string: String): IntArray {
        val split = string.split(" [", ", ", "]")
        return intArrayOf(split[1].toInt(), split[2].toInt(), split[3].toInt(), split[4].toInt())
    }

    private fun parseInstruction(string: String): IntArray {
        val split = string.split(" ")
        return intArrayOf(split[0].toInt(), split[1].toInt(), split[2].toInt(), split[3].toInt())
    }

    fun addr(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = output[a] + output[b]
        return output
    }

    fun addi(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = output[a] + b
        return output
    }

    fun mulr(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = output[a] * output[b]
        return output
    }

    fun muli(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = output[a] * b
        return output
    }

    fun banr(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = output[a] and output[b]
        return output
    }

    fun bani(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = output[a] and b
        return output
    }

    fun borr(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = output[a] or output[b]
        return output
    }

    fun bori(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = output[a] or b
        return output
    }

    fun setr(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = output[a]
        return output
    }

    fun seti(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        output[c] = a
        return output
    }

    fun gtir(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        if (a > output[b]) {
            output[c] = 1
        } else {
            output[c] = 0
        }
        return output
    }

    fun gtri(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        if (output[a] > b) {
            output[c] = 1
        } else {
            output[c] = 0
        }
        return output
    }

    fun gtrr(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        if (output[a] > output[b]) {
            output[c] = 1
        } else {
            output[c] = 0
        }
        return output
    }

    fun eqir(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        if (a == output[b]) {
            output[c] = 1
        } else {
            output[c] = 0
        }
        return output
    }

    fun eqri(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        if (output[a] == b) {
            output[c] = 1
        } else {
            output[c] = 0
        }
        return output
    }

    fun eqrr(input: IntArray, instruction: IntArray): IntArray {
        val output = input.copyOf()
        val a = instruction[1]
        val b = instruction[2]
        val c = instruction[3]
        if (output[a] == output[b]) {
            output[c] = 1
        } else {
            output[c] = 0
        }
        return output
    }
}